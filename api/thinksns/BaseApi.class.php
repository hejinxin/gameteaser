<?php
require('s_function.php');
class BaseApi extends Api{
	
	public function response($code,$statusTag,$statusMsg,$data){
		//$this->setHeaderForCor();
		header("Content-type: application/json; charset=utf-8");
		exit (json_encode(array(	'code'=>$code,
									'statusTag'=>$statusTag,
									'statusMsg'=>$statusMsg,
									'data'=>$data
									)));
	}
	
	public function verifyError(){
		//$this->setHeaderForCor();
		header("Content-type: application/json; charset=utf-8");
		$this->response('0002','','verify failed');
	}

	public function setHeaderForCor(){
		header('Access-Control-Allow-Origin: http://localhost:63342');
		header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
		header('Access-Control-Max-Age: 3600');
		header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers,Access-Control-Allow-Methods,Access-Control-Max-Age, Authorization, X-Requested-With,Access-Control-Allow-Origin');
		header('Access-Control-Allow-Credentials: true');
	}


	public function verifyOauth($oauth_token,$oauth_token_secret,$type="location"){
		$verifycode = array();
		$verifycode['oauth_token'] = $oauth_token;
		$verifycode['oauth_token_secret'] = $oauth_token_secret;
		$verifycode['type'] = $type;
		$login = D('Login')->where($verifycode)->field('uid,oauth_token,oauth_token_secret')->find();
		return $login;
	}

	public function is_login(){
		$this->login = $this->verifyOauth($this->data['oauth_token'],$this->data['oauth_token_secret']);
		return $this->login;
	}

}