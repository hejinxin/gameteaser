<?php
require('BaseApi.class.php');
class OauthApi extends BaseApi{
	
	var $passport;
	
	private $_config;					// 注册配置信息字段
	private $_register_model;			// 注册模型字段
	private $_user_model;				// 用户模型字段
	private $_invite;					// 是否是邀请注册
	private $_invite_code;				// 邀请码
	/**
	 * 模块初始化
	 * @return void
	 */
	protected function _initialize() {
		$this->passport = model('Passport');
		$this->_config = model('Xdata')->get('admin_Config:register');
		$this->_user_model = model('User');
		$this->_register_model = model('Register');
	}
	
	public function request_key(){
		$this->response("0","","",C('API_KEY'));
	}
	
	public function save_oauth($user){
		//生成 oauth_token，oauth_token_secret 写入app 登录记录
		$rand = rand(1000,9999);
		$oauth_token = md5($user['uid'].$rand);
		$oauth_token_secret = md5($user['password'].$rand);
		
		$map = array();
		$map['uid'] = $user['uid'];
		$map['type'] = 'location';//thinsns默认值
		$data = D('login')->where($map)->find();
		if($data['login_id']>0){
			$data['type_uid'] = $user['uname'];
			$data['oauth_token'] = $oauth_token;
			$data['oauth_token_secret'] = $oauth_token_secret;
			$res = D('login')->save($data);
		}else{
			$data = array();
			$data['uid'] = $user['uid'];
			$data['type_uid'] = $user['uname'];
			$data['type'] = 'location';//thinsns默认值
			$data['oauth_token'] = $oauth_token;
			$data['oauth_token_secret'] = $oauth_token_secret;
			$data['is_sync'] = 0;
			
			$res = D('login')->add($data);
		}
		
		if($res){
			$auth = array();
			$auth['uid'] = $user['uid'];
			$auth['oauth_token'] = $data['oauth_token'];
			$auth['oauth_token_secret'] = $data['oauth_token_secret'];
			
			return $auth;
		}else{
			return false;
		}
	}
	
	public function authorize(){
		$uid = $_POST['uid'];//$this->passport->jiemi(h('uid'));
		$passwd = $_POST['passwd'];//$this->passport->jiemi(h('passwd'));
		
		//用户表验证用户名密码
		$user = $this->passport->getLocalUser($uid,$passwd);
		
		//dump($user);
		//echo $this->passport->getError();
		if($user){
			$oauth = $this->save_oauth($user);
			
			if($oauth){
				//返回 oauth_token，oauth_token_secret
				$this->response("0",L("login_success"),"",$oauth);
			}else{
				$this->response("020101","",L("login_failed_network_problem"),null);
			}
		}else{
			//登录失败
			$this->response("020102","",L("login_failed"),null);
		}		
		
	}

	public function register(){
		$invite = t($_POST['invate']);
		$inviteCode = t($_POST['invate_key']);
		$email = t($_POST['email']);
		$uname = $email;//t($_POST['uname']);客户端注册无用户名选项，用email代替
		$sex = 1 == $_POST['sex'] ? 1 : 2;
		$password = trim($_POST['passwd']);
		//$repassword = trim($_POST['repassword']);

		//检查验证码
		/*if (md5(strtoupper($_POST['verify'])) != $_SESSION['verify']) {
			$this->error('验证码错误');
		}*/
		
		/*if(!$this->_register_model->isValidName($uname)) {
			$this->response("020201","uname",$this->_register_model->getLastError(),null);
		}*/

		if(!$this->_register_model->isValidEmail($email)) {
			$this->response("020202","email",$this->_register_model->getLastError(),null);
		}

		if(!$this->_register_model->isValidPassword($password, $password)){
			$this->response("020203","passwd",$this->_register_model->getLastError(),null);
		}
		
		// if (!$_POST['accept_service']) {
		// 	$this->error(L('PUBLIC_ACCEPT_SERVICE_TERMS'));
		// }

		$login_salt = rand(11111, 99999);
		$map['uname'] = $uname;
		$map['sex'] = $sex;
		$map['login_salt'] = $login_salt;
		$map['password'] = md5(md5($password).$login_salt);
		$map['login'] = $map['email'] = $email;
		$map['reg_ip'] = get_client_ip();
		$map['ctime'] = time();

		// 添加地区信息
		$map['location'] = t($_POST['city_names']);
		$cityIds = t($_POST['city_ids']);
		$cityIds = explode(',', $cityIds);
		isset($cityIds[0]) && $map['province'] = intval($cityIds[0]);
		isset($cityIds[1]) && $map['city'] = intval($cityIds[1]);
		isset($cityIds[2]) && $map['area'] = intval($cityIds[2]);
		// 审核状态： 0-需要审核；1-通过审核
		$map['is_audit'] = $this->_config['register_audit'] ? 0 : 1;
		// 需求添加 - 若后台没有填写邮件配置，将直接过滤掉激活操作
		$isActive = $this->_config['need_active'] ? 0 : 1;
		if ($isActive == 0) {
			$emailConf = model('Xdata')->get('admin_Config:email');
			if (empty($emailConf['email_host']) || empty($emailConf['email_account']) || empty($emailConf['email_password'])) {
				$isActive = 1;
			}
		}
		$map['is_active'] = $isActive;
		$map['first_letter'] = getFirstLetter($uname);
		//如果包含中文将中文翻译成拼音
		if ( preg_match('/[\x7f-\xff]+/', $map['uname'] ) ){
			//昵称和呢称拼音保存到搜索字段
			$map['search_key'] = $map['uname'].' '.model('PinYin')->Pinyin( $map['uname'] );
		} else {
			$map['search_key'] = $map['uname'];
		}
				
		//邮箱判断是否已经注册
		$user = $this->_user_model->where("email='".$email."'")->find();
		//dump($user);
		if($user['uid']>0){
			$this->response("020201","email",L("email_already_registered"),null);
		}
		//手机号码判断是否已经注册
		$user = $this->_user_model->where("phone='".$phone."'")->find();
		//dump($user);
		if($user['uid']>0){
			$this->response("020202","phone",L("phone_already_registered"),null);
		}
		
		$uid = $this->_user_model->add($map);
		if($uid) {
			// 添加积分
			model('Credit')->setUserCredit($uid,'init_default');
			// 如果是邀请注册，则邀请码失效
			if($invite) {
				$receiverInfo = model('User')->getUserInfo($uid);
				// 验证码使用
				model('Invite')->setInviteCodeUsed($inviteCode, $receiverInfo);
				// 添加用户邀请码字段
				model('User')->where('uid='.$uid)->setField('invite_code', $inviteCode);
				//给邀请人奖励
			}

			// 添加至默认的用户组
			$userGroup = model('Xdata')->get('admin_Config:register');
			$userGroup = empty($userGroup['default_user_group']) ? C('DEFAULT_GROUP_ID') : $userGroup['default_user_group'];
			model('UserGroupLink')->domoveUsergroup($uid, implode(',', $userGroup));

			//注册来源-第三方帐号绑定
			if(isset($_POST['other_type'])){
				$other['type'] = t($_POST['other_type']);
				$other['type_uid'] = t($_POST['other_uid']);	
				$other['oauth_token'] = t($_POST['oauth_token']);
				$other['oauth_token_secret'] = t($_POST['oauth_token_secret']);
				$other['uid'] = $uid;
				D('login')->add($other);
			}

			//判断是否需要审核
			if($this->_config['register_audit']) {
				//$this->redirect('public/Register/waitForAudit', array('uid' => $uid));
				$this->response("020203","account",L("register_need_audit"),null);
			} else {
				if(!$isActive){
					$this->_register_model->sendActivationEmail($uid);
					$this->response("020204","account",L("wait_for_activation"),null);
				}else{
					//D('Passport')->loginLocal($email,$password);
					$user = $this->_user_model->where("uid=".$uid)->find();
					$oauth = $this->save_oauth($user);
					
					$this->response("0","",L("register_success"),$oauth);
				}
			}

		} else {
			$this->response("020205","phone",L("PUBLIC_REGISTER_FAIL"),null);
		}
		
		
	}
	
	public function request_phone_code(){
		$phone = $_POST['phone'];
		$type = $_POST['type'];
		if(empty($phone)){
			$this->response("020301",L("phone_number_empty"),"",false);
		}
		
		if(empty($type)){
			$this->response("020303",L("missed_type_parameter"),"",false);
		}
		
		$s_verify_code_data = S('verrify_code_'.$phone."_".$type);
		if(!empty($s_verify_code_data)&&time()-$s_verify_code_data['add_time']>60){
			$this->response("020304",L("verify_code_in_cool_donw"),"",false);
		}		
		//查询用户信息，判断是否是注册用户
		$user = $this->_user_model->where("phone='".$phone."'")->find();
		if($user['uid']>0){
			$verify_code = "888888";
			//call sms api 
			$data = array();
			$data['verify_code'] = $verify_code;
			$data['type'] = $type;
			$data['add_time'] = time();
			//保存验证码到缓存中
			S('verrify_code_'.$phone."_".$type,$data,1800);
			$this->response("0","",L("verify_code_sent"),true);
		}else{
			$this->response("020302","",L("phone_number_not_found"),false);
		}
	}
	
	public function phone_verify(){
		$phone = $_POST['phone'];
		$verify_code = $_POST['verify_code'];
		if(empty($phone)){
			$this->response("020701","",L("phone_number_empty"),false);
		}
		
		if(empty($verify_code)){
			$this->response("020702","",L("verify_code_empty"),false);
		}
		
		$type = "phone_verify";
		$s_verify_code_data = S('verrify_code_'.$phone."_".$type);
		if(!empty($s_verify_code_data)&&$s_verify_code_data['verify_code']==$verify_code){
			//验证成功
			$res = $this->_user_model->where("phone='".$phone."'")->setField("phone_verified",1);
			$this->response("0","",L("verify_code_ok"),true);
		}else{
			$this->response("020703","",L("verify_code_wrong"),false);
		}
	}
	
	public function reset_password(){
		$_POST["email"]	= t($_POST["email"]);
		if(!$this->_isEmailString($_POST['email'])) {
			$this->response("020401","",L("PUBLIC_EMAIL_TYPE_WRONG"),false);
		}

		$user =	model("User")->where('`email`="'.$_POST["email"].'"')->find();
        if(!$user) {
        	$this->response("020402","",L("email_not_registered"),false);        
		} 
		
		$result = $this->_send_password_email($user);
		if($result){
			$this->response("0","",L("email_sent"),true);
		}else{
			$this->response("020403","",L("failed_to_send_email"),false);
		}
	}
	
	private function _isEmailString($email) {
		return preg_match("/[_a-zA-Z\d\-\.]+@[_a-zA-Z\d\-]+(\.[_a-zA-Z\d\-]+)+$/i", $email) !== 0;
	}
	
	private function _send_password_email($user) {
		if($user['uid']) {
	    	//$this->appCssList[] = 'login.css';		// 添加样式
	        $code = md5($user["uid"].'+'.$user["password"].'+'.rand(1111,9999));
	        $config['reseturl'] = U('api/Oauth/do_reset_password', array('code'=>$code));
	        //设置旧的code过期
	        D('FindPassword')->where('uid='.$user["uid"])->setField('is_used',1);
	        //添加新的修改密码code
	        $add['uid'] = $user['uid'];
	        $add['email'] = $user['email'];
	        $add['code'] = $code;
	        $add['is_used'] = 0;
	        $result = D('FindPassword')->add($add);
	        if($result){
	    		model('Notify')->sendNotify($user['uid'], 'password_reset', $config);
				return true;
			}else{
				return false;
			}
	    }
	}
	
	public function do_reset_password(){
		$code = t($_GET['code']);
		$map['code'] = $code;
		$map['is_used'] = 0;
		$uid = D('find_password')->where($map)->getField('uid');
		if(!$uid){
			$msg = L("The link for reset the password is out of date,please try again the app");
			exit($msg);
		}
		$user = model('User')->where("`uid`={$uid}")->find();
		
		if (!$user) {
			$msg = L("user record to found");
			exit($msg);
		}else{
			//随机生成新的密码
			$new_password = substr(md5(rand(100000,999999)),0,8);
			$map1['uid'] = $user['uid'];
			$data = array();
			$data['login_salt'] = rand(10000,99999);
			$data['password']   = md5(md5($new_password) . $data['login_salt']);
			$res = model('User')->where($map1)->save($data);
			//设置已用
			$uid = D('find_password')->where($map)->setfield('is_used',1);
			echo L("Your password has been reset. Your new paasword is ").$new_password;
			exit;
		}
	}
	
	public function set_password(){
		$email = $_POST['email'];
		$passwd = $_POST['passwd'];
		$new_passwd = $_POST['new_passwd'];
		
		//用户表验证用户名密码
		$user = $this->passport->getLocalUser($email,$passwd);
		
		if($user){
			if(!$this->_register_model->isValidPassword($new_passwd, $new_passwd)){
				$this->response("020501","passwd",$this->_register_model->getLastError(),null);
			}
			//更新密码	
			$map1['uid'] = $user['uid'];
			$data = array();
			$data['login_salt'] = rand(10000,99999);
			$data['password']   = md5(md5($new_passwd) . $data['login_salt']);
			$res = model('User')->where($map1)->save($data);
			if($res){
				//设置app端 登录记录失效
				
				$this->response("0","",L("password changed"),true);
			}else{
				$this->response("020503","passwd",L("network problem"),false);
			}
		}else{
			//登录失败
			$this->response("020502","passwd",L("password not correct"),false);
		}		
	}
	
	public function logout(){
		$oauth_token = $_REQUEST['oauth_token'];
		$oauth_token_secret = $_REQUEST['oauth_token_secret'];
		
		//判断oauth是否正确
		$login = $this->verifyOauth($oauth_token,$oauth_token_secret);
		if($login){
			//删除app端登录记录
			D('Login')->where($login)->delete();
			$this->response("0","",L("logout success"),true);
		}else{
			$this->response("020601","oauth",L("oauth verify failed"),false);
		}
		
	}
}