<?php
/**
 * Created by PhpStorm.
 * User: sami
 * Date: 15/12/5
 * Time: 下午11:00
 */
require('BaseApi.class.php');
require(SITE_PATH.'/apps/weiba/Lib/Model/WeibaModel.class.php');
require(SITE_PATH.'/apps/weiba/Lib/Model/WeibaPostModel.class.php');
require(SITE_PATH.'/apps/weiba/Lib/Model/WeibaReplyModel.class.php');
require(SITE_PATH.'/apps/weiba/Lib/Model/WeibaReplyDiggModel.class.php');
class PostApi extends BaseApi{

    /**
     * 模块初始化
     * @return void
     */
    protected function _initialize() {
        //parent::_initialize();
    }

    public function post_timeline(){
        $post_model = new WeibaPostModel();
        $data = $post_model->post_timeline($this->data['weiba_id'], $this->since_id, $this->max_id, $this->count, $this->page);
        $this->response("0","","",$data);
    }

    public function post_detail(){
        $post_model = new WeibaPostModel();
        $post_detail = $post_model->getPostInfo($this->id);
        $post_reply_model = new WeibaReplyModel();
        $post_reply_digg_model = new WeibaReplyDiggModel();
        //获取评论 10 条
        $post_detail['comments'] = $post_reply_model->getReplyListForGt(array('post_id'=>$post_detail['post_id']),$order = 'reply_id desc', $limit = 10);
        $this->response("0","","",$post_detail);
    }

    public function comment_list(){
        $post_reply_model = new WeibaReplyModel();
        $comments = $post_reply_model->getReplyListForGt(array('post_id' => $this->id), 'reply_id desc', $this->count);
        $this->response("0","","",$comments);
    }

    public function like_list(){
        $post_model = new WeibaPostModel();
        $diggs = $post_model->getPostDiggs($this->id, $this->count);
        $this->response("0","","",$diggs);
    }

    public function comment(){
        //检查登录状态
        if($_SESSION['mid']>0) {
            $reply_model = new WeibaReplyModel();
            $res = $reply_model->addReplyForApi($this->id,$this->data['content'],$_SESSION['mid']);
            if($res){
                $this->response("0","","",true);
            }else{
                $this->response("0001","","Add comment failed.Please try again later.",false);
            }
        }else{
            $this->verifyError();
        }
    }

    public function like(){
        //检查登录状态
        if($_SESSION['mid']>0) {
            $post_model = new WeibaPostModel();
            $res = $post_model->addPostDigg($this->id,$_SESSION['mid']);
            if($res){
                $this->response("0","","",true);
            }else{
                $this->response("0001","","Add like failed..",false);
            }
        }else{
            $this->verifyError();
        }
    }

    public function delete_like(){
        //检查登录状态
        if($_SESSION['mid']>0) {
            $post_model = new WeibaPostModel();
            $res = $post_model->delPostDigg($this->id,$_SESSION['mid']);
            if($res){
                $this->response("0","","",true);
            }else{
                $this->response("0001","","Delete like failed..",false);
            }
        }else{
            $this->verifyError();
        }
    }

    public function video_list(){
        $game_video_model = M('game_video');
        $list = $game_video_model->where(1)->page($this->page,$this->count)->select();
        $this->response("0","","",$list);
    }

    public function game_list(){
        $weiba_model = new WeibaModel();
        $list = $weiba_model->getWeibaList(9999);

        for($i=0;$i<count($list['data']);$i++){
            unset($list['data'][$i]['uid']);
            unset($list['data'][$i]['who_can_post']);
            unset($list['data'][$i]['who_can_reply']);
            unset($list['data'][$i]['admin_uid']);
            unset($list['data'][$i]['follower_count/thread_count']);
            unset($list['data'][$i]['DOACTION']);
        }

        $this->response("0","","",$list['data']);
    }


    public function game(){
        $weiba_model = new WeibaModel();
        $weiba = $weiba_model->getWeibaById($this->id);
        $post_list = $weiba_model->getPostListGt(20, array('weiba_id' => $this->id),true);
        if(!empty($weiba)){
            $this->response("0","","",array('weiba'=>$weiba,'post_list'=>$post_list));
        }else{
            $this->response("030000","","",null);
        }
    }

    public function gift(){
        $model = M();
        $model->query('USE `tzula`');
        $sql = "SELECT nnrl6_cmlivedeal_deals.*,nnrl6_cmlivedeal_images.`file_name`,nnrl6_cmlivedeal_images.`user_id` AS image_user_id FROM `nnrl6_cmlivedeal_deals` LEFT JOIN `nnrl6_cmlivedeal_images` ON `nnrl6_cmlivedeal_deals`.`image_id` = `nnrl6_cmlivedeal_images`.`id` WHERE published=1 AND approved=1";
        $list = $model->query($sql);
        $gifts = array();
        for($i=0;$i<count($list);$i++){
            $gifts[] = array(
                'gift_id'           =>$list[$i]['id'],
                'title'             =>$list[$i]['title'],
                'description'       =>$list[$i]['description'],
                'starting_time'     =>$list[$i]['starting_time'],
                'ending_time'       =>$list[$i]['ending_time'],
                'coupon_quantity'   =>$list[$i]['coupon_quantity'],
                'image'             =>"http://www.gameteaser.com/images/Homepagebanner/".$list[$i]['image_user_id']."/".$list[$i]['file_name']
            );
        }

        $this->response("0","","",$gifts);
    }

    public function request_gift(){
        //判断登录 TODO

        $gift_id = intval($this->data['gift_id']);
        $model = M();
        $model->query('USE `tzula`');
        $sql = "SELECT count(id) AS total FROM nnrl6_cmlivedeal_coupons WHERE redeemed=0 AND deal_id=".$gift_id;
        //查询对应的礼品是否还有余量
        $r = $model->query($sql);
        $left = $r[0]['total'];
        if($left>0){
            //获取对应的礼品券
            $sql = "SELECT * FROM nnrl6_cmlivedeal_coupons WHERE redeemed=0 AND deal_id=".$gift_id." LIMIT 1";
            $r = $model->query($sql);
            $data = $r[0];
            //更新礼品券状态
            $sql = "UPDATE nnrl6_cmlivedeal_coupons SET redeemed=1,user_id=".$this->mid.",redeemed_time='".date("Y-m-d h:i:s")."' WHERE id=".$data['id']." AND redeemed=0";
            $r = $model->execute($sql);
            if($r){
                //加入日志记录 TODO

                //更新礼品余量 -1
                $sql = "UPDATE nnrl6_cmlivedeal_deals SET coupon_quantity=coupon_quantity-1 WHERE id=".$gift_id." LIMIT 1";
                $gift = array(
                    'gift_id'=>$gift_id,
                    'coupon'=>$data['code']
                );
                $this->response("0","","",$gift);
            }else{
                $this->response("0303031","","No more gift left",null);
            }
        }else{
            $this->response("0303030","","No more gift left",null);
        }


    }

    public function test(){
        /*$Db = new Db();
        $r = $Db->addConnect(C('DB_CONFIG_TZULA'),2);
        $Db->switchConnect(2);
        $list = $Db->query("SELECT * FROM `tzula`.nnrl6_cmlivedeal_deals");
        dump($list);
        dump($Db->getLastSql());
        dump($Db->error());*/

        $model = M();
        $model->query('USE `tzula`');
        $sql = "SELECT nnrl6_cmlivedeal_deals.*,nnrl6_cmlivedeal_images.`file_name`,nnrl6_cmlivedeal_images.`user_id` AS image_user_id FROM `nnrl6_cmlivedeal_deals` LEFT JOIN `nnrl6_cmlivedeal_images` ON `nnrl6_cmlivedeal_deals`.`image_id` = `nnrl6_cmlivedeal_images`.`id`";
        $list = $model->query($sql);

        dump($list);
        die;
    }



}