<?php
/**
 * Created by PhpStorm.
 * User: sami
 * Date: 15/12/9
 * Time: 下午3:11
 */
require('BaseApi.class.php');
require(SITE_PATH.'/apps/weiba/Lib/Model/WeibaModel.class.php');
require(SITE_PATH.'/apps/weiba/Lib/Model/WeibaPostModel.class.php');
require(SITE_PATH.'/apps/weiba/Lib/Model/WeibaReplyModel.class.php');
require(SITE_PATH.'/apps/weiba/Lib/Model/WeibaReplyDiggModel.class.php');
class CorporationApi extends BaseApi{

    /**
     * 模块初始化
     * @return void
     */
    protected function _initialize(){
        $this->is_login();
        //检查是否是游戏商
        //TODO

    }

    public function check_permission(){
        $login = $this->verifyOauth($this->data['oauth_token'],$this->data['oauth_token_secret']);
        dump($login);
        $user = Model('User')->getUserInfo($login['uid']);
        dump($user);


    }

    //发表
    public function create_post(){
        $weiba_id = intval($_POST['weiba_id']);
        if ($weiba_id==0){
            $this->response("030000","weiba_id","Please choose the game",null);
        }

        if(empty(t($_POST['title']))){
            $this->response("030000","title","Please input title",null);
        }

        if(empty(h($_POST['content']))){
            $this->response("030000","title","Please input content",null);
        }

        //检查当前用户是否是 该微吧的管理员
        $weiba_model = new WeibaModel();
        $post_model = new WeibaPostModel();
        $weiba = $weiba_model->getWeibaById($weiba_id);

        if($this->login['uid']!=$weiba['admin_uid']){
            $this->response("030000","weiba_id","Only the administrator of this game can create post.",null);
        }

        if(!empty($_FILES)){
            $attachInfo = $this->save();
            if($attachInfo['status']==1){
                $data['attach'] = serialize(array($attachInfo['data']['attach_id']));
            }
        }

        $data['weiba_id']           = $weiba_id;
        $data['title']              = t($_POST['title']);
        $data['content']            = h($_POST['content']);
        $data['post_uid']           = $this->login['uid'];
        $data['post_time']          = time();
        $data['last_reply_uid']     = $this->login['uid'];
        $data['last_reply_time']    = $data['post_time'];

        $filterTitleStatus = filter_words($data['title']);
        if (!$filterTitleStatus['status']) {
            $this->response("030000","title","标题包含不文明词语",null);
        }
        $data['title'] = $filterTitleStatus['data'];

        $filterContentStatus = filter_words($data['content']);
        if (!$filterContentStatus['status']) {
            $this->response("030000","title","内容包含不文明词语",null);
        }
        $data['content'] = $filterContentStatus['data'];

        $res = D('weiba_post')->add($data);
        if($res){
            $weiba_model->setNewcount($weiba_id);
            $weiba_model->where('weiba_id='.$data['weiba_id'])->setInc('thread_count');
            //同步到分享
            $feed_id = model('Feed')->syncToFeed('weiba', $this->login['uid'], $res);
            $post_model->where('post_id='.$res)->setField('feed_id',$feed_id);

            $result['id'] = $res;
            $result['feed_id'] = $feed_id;
            //添加积分
            model('Credit')->setUserCredit($this->login['uid'],'publish_topic');
            //更新发帖数
            D('UserData')->updateKey('weiba_topic_count',1);
            $this->response("0","","",$result['id']);
        }else{
            $this->response("0","","发布失败，等待返回修改发布",null);
        }

    }


    /**
     * 附件上传
     * @return array 上传的附件的信息
     */
    public function save($attach_type='weiba_attach',$upload_type='file'){

        $data['attach_type'] = $attach_type;
        $data['upload_type'] = $upload_type;
        //$thumb  = intval($_REQUEST['thumb']);
        $thumb = 1;
        $width  = intval($_REQUEST['width']);
        $height = intval($_REQUEST['height']);
        $cut    = intval($_REQUEST['cut']);

        //Addons::hook('widget_upload_before_save', &$data);

        $option['attach_type'] = $data['attach_type'];
        $info = model('Attach')->upload($data, $option);
        //Addons::hook('widget_upload_after_save', &$info);

        if($info['status']){
            $data = $info['info'][0];
            if($thumb==1){
                $data['src'] = getImageUrl($data['save_path'].$data['save_name'],$width,$height,$cut);
            }else{
                $data['src'] = $data['save_path'].$data['save_name'];
            }

            $data['extension']  = strtolower($data['extension']);
            $return = array('status'=>1,'data'=>$data);
        }else{
            $return = array('status'=>0,'data'=>$info['info']);
        }

        return $return;
    }

    public function my_game(){
        $weiba_model = new WeibaModel();
        $list = $weiba_model->getWeibaList(9999,array('admin_uid'=>$this->login['uid']));

        for($i=0;$i<count($list['data']);$i++){
            unset($list['data'][$i]['uid']);
            unset($list['data'][$i]['who_can_post']);
            unset($list['data'][$i]['who_can_reply']);
            unset($list['data'][$i]['admin_uid']);
            unset($list['data'][$i]['follower_count/thread_count']);
            unset($list['data'][$i]['DOACTION']);
        }

        $this->response("0","","",$list['data']);
    }

    public function add_game(){
        $weibaAuditConfig = model('Xdata')->get('weiba_Admin:weibaAuditConfig');
        $data['weiba_name'] = t($_POST['weiba_name']);
        $data['cid'] = intval($_POST['cid']);
        $data['intro'] = t($_POST['intro']);
        $data['who_can_post'] = 3;

        if(strlen($data['weiba_name']) == 0){
            $this->response("1","weiba_name","微吧名称不能为空",false);
        }

        if($data['cid']==0){
            $this->response("1","cid","微吧分类不能为空",false);
        }

        if(strlen($data['intro']) == 0){
            $this->response("1","intro","简介不能为空",false);
        }

        // $data['info'] = t($_POST['info']);
        //游戏logo处理

        if(!empty($_FILES)){
            $attachInfo = $this->save('avatar','image');
            if($attachInfo['status']==1){
                $data['avatar_big'] = $attachInfo['data']['src'];
                $data['avatar_middle'] = $attachInfo['data']['src'];
            }
        }

        $data['uid'] = $this->login['uid'];
        $data['ctime'] = time();
        $data['admin_uid'] = $this->login['uid'];
        $data['follower_count'] = 1;
        $data['status'] = 0;
        $res = D('Weiba','weiba')->add($data);
        if($res){
            $follow['follower_uid'] = $this->login['uid'];
            $follow['weiba_id'] = $res;
            $follow['level'] = 3;
            D('weiba_follow')->add($follow);
            $this->response("0","","申请成功,请等待审核",true);
        }else{
            $this->response("1","","申请失败",false);
        }
    }

    public function update_game(){
        //判断是否是自己管理游戏主页
        $weiba_id = intval($_POST['weiba_id']);
        if ($weiba_id==0){
            $this->response("030000","weiba_id","Please choose the game",null);
        }

        //检查当前用户是否是 该微吧的管理员
        $weiba_model = new WeibaModel();
        $weiba = $weiba_model->getWeibaById($weiba_id);

        if($this->login['uid']!=$weiba['admin_uid']){
            $this->response("030000","weiba_id","Only the administrator of this game can edit the information.",null);
        }

        $data['weiba_name'] = t($_POST['weiba_name']);
        $data['intro'] = t($_POST['intro']);
        $data['logo'] = t($_POST['logo']);
        $data['cid'] = t($_POST['cid']);
        //$data['who_can_post'] = t($_POST['who_can_post']);
        //$data['info'] = t($_POST['info']);
        $weiba = M('weiba')->where('weiba_id='.$weiba_id)->find();
        if(!empty($_FILES)){
            $attachInfo = $this->save('avatar','image');
            if($attachInfo['status']==1){
                $data['avatar_big'] = $attachInfo['data']['src'];
                $data['avatar_middle'] = $attachInfo['data']['src'];
            }
        }

        if(strlen($data['weiba_name']) == 0){
            $this->response("1","weiba_name","微吧名称不能为空",false);
        }

        if($data['cid']==0){
            $this->response("1","cid","微吧分类不能为空",false);
        }

        if(strlen($data['intro']) == 0){
            $this->response("1","intro","简介不能为空",false);
        }

        $res = M('weiba')->where('weiba_id='.$weiba_id)->save($data);
        //dump(M()->getLastSql());
        //dump($res);exit;
        if($res !== false){
            //D('log')->writeLog($weiba_id,$this->mid,'修改了微吧基本信息','setting');
            $this->response("0","","修改了微吧基本信息",true);
        }else{
            $this->response("0","","保存失败",false);
        }

    }
}