<?php
/**
 * 微吧模型 - 数据对象模型
 * @author jason <yangjs17@yeah.net> 
 * @version TS3.0
 */
class WeibaPostModel extends Model {

	protected $tableName = 'weiba_post';
	protected $error = '';
	protected $fields = array(
							0 =>'post_id',1=>'weiba_id',2=>'post_uid',3=>'title',4=>'content',5=>'post_time',
							6=>'reply_count',7=>'read_count',8=>'last_reply_uid',9=>'last_reply_time',10=>'digest',11=>'top',12=>'lock',
							13=>'api_key',14=>'domain',15=>'is_index',16=>'index_img',17=>'reg_ip',
							18=>'is_del',19=>'feed_id',20=>'reply_all_count',21=>'attach',22=>'form',23=>'top_time',24=>'is_index_time','_autoinc'=>true,'_pk'=>'post_id'
						);

	protected $cache_prefix = 'weiba_post_';

	/**
	 * 发帖同步到分享
	 * @param integer post_id 帖子ID
	 * @param string title 帖子标题
	 * @param string content 帖子内容
	 * @param integer uid 发布者uid
	 * @return integer feed_id 分享ID
	 */
	public function syncToFeed($post_id,$title,$content,$uid) {	
		$d['content'] = '';
		$d['body'] = '【'.$title.'】'.getShort($content,100).'&nbsp;';
		$feed = model('Feed')->put($uid, 'weiba', 'weiba_post', $d, $post_id, 'weiba_post');
		return $feed['feed_id'];
	}

	/**
	 * 发表帖子forapi
	 * @param integer weiba_id 微吧ID
	 * @param varchar title 帖子标题
	 * @param varchar content 帖子内容
	 * @param integer user_id 帖子作者 
	 */
	public function createPostForApi($weiba_id,$title,$content,$uid){
		$data['weiba_id'] = intval($weiba_id);
		$data['title'] = t($title);
		$data['content'] = h($content);
		$data['post_uid'] = intval($uid);
		$data['post_time'] = time();
		$data['last_reply_time'] = $data['post_time'];
		$res = D('weiba_post')->add($data);
		if($res){
			D('weiba')->where('weiba_id='.$data['weiba_id'])->setInc('thread_count');
			//同步到分享
			$feed_id = $this->syncToFeed($res,$data['title'],$data['content'],$data['post_uid']);
			D('weiba_post')->where('post_id='.$res)->setField('feed_id',$feed_id);
			return true;
		}else{
			return false;
		}
	}

	/**
	 * 收藏帖子
	 * @param integer post_id 帖子ID
	 */
	public function favoriteForApi($post_id){
		$postDetail = D('weiba_post')->where('post_id='.intval($post_id))->find();
		$data['post_id'] = intval($post_id);
		$data['weiba_id'] = $postDetail['weiba_id'];
		$data['post_uid'] = $postDetail['post_uid'];
		$data['uid'] = $GLOBALS['ts']['mid'];
		$data['favorite_time'] = time();
		if(D('weiba_favorite')->add($data)){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * 取消收藏帖子
	 * @param integer post_id 帖子ID
	 */
	public function unfavoriteForApi($post_id){
		$map['post_id'] = intval($post_id);
		$map['uid'] = $GLOBALS['ts']['mid'];
		if(D('weiba_favorite')->where($map)->delete()){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * 为feed提供应用数据来源信息 - 与模板weiba_post.feed.php配合使用
	 * @param integer row_id 帖子ID
	 * @param bool _forApi 提供给API的数据
	 */
	public function getSourceInfo($row_id, $_forApi = false){
		$info =  $this->find($row_id);
		if(!$info) return false;
		$info['source_user_info'] = model('User')->getUserInfo($info['post_uid']);
		$info['source_user'] = $info['post_uid'] == $GLOBALS['ts']['mid'] ? L('PUBLIC_ME'): $info['source_user_info']['space_link'];			// 我
		$info['source_type'] = L('PUBLIC_WEIBA');
		$info['source_title'] = $_forApi ? parseForApi($info['source_user_info']['space_link']) : $info['source_user_info']['space_link'];	//分享title暂时为空
		$info['source_url'] = U('weiba/Index/postDetail', array('post_id'=>$row_id));
		$info['ctime'] = $info['post_time'];
		$feed = D('feed_data')->field('feed_id,feed_content')->find($info['feed_id']);
		$info['source_content'] = $feed['feed_content'];
		$info['app_row_table'] = 'weiba_post';
		$info['app_row_id'] = $info['post_id'];
		return $info;
	}


	public function getPostInfo($row_id,$from_cache=true){
		$post =false;
		if($from_cache){
			$post = model('Cache')->get($this->cache_prefix.$row_id);
		}

		if($post==false){
			$post =  $this->find($row_id);
			$attachIdArray = unserialize($post['attach']);
			$ids = implode(",",$attachIdArray);
			$attach = array();
			$attaches = model('Attach')->getAttachByIds($ids);
			for($i=0;$i<count($attaches);$i++){
				$filename = $attaches[$i]['save_path'].$attaches[$i]['save_name'];
				$attach[] = UPLOAD_URL.'/'.$filename;
			}

			$post['attach'] = $attach;
			$user_info = model('User')->getUserInfo($post['post_uid']);
			$post['uname'] =$user_info['uname'];
			$post['avatar_big'] =$user_info['avatar_big'];
			$post['avatar_middle'] =$user_info['avatar_middle'];
			$post['avatar_small'] =$user_info['avatar_small'];
			model('Cache')->set($this->cache_prefix.$row_id,$post);
		}

		return $post;

	}

	public function post_timeline($weiba_id=0, $since_id = 0, $max_id = 0 ,$limit = 20 ,$page = 1,$returnId=false) {
		$weiba_id = intval($weiba_id);
		$since_id = intval($since_id);
		$max_id = intval($max_id);
		$limit = intval($limit);
		$page = intval($page);
		$where = " is_del = 0 ";
		//动态类型
		if($weiba_id>0){
			$where .= " AND weiba_id='$weiba_id' ";
		}
		if(!empty($since_id) || !empty($max_id)) {
			!empty($since_id) && $where .= " AND post_id > {$since_id}";
			!empty($max_id) && $where .= " AND post_id < {$max_id}";
		}
		$start = ($page - 1) * $limit;
		$end = $limit;
		$post_ids = $this->where($where)->field('post_id')->limit("{$start},{$end}")->order('post_id DESC')->getAsFieldArray('post_id');
		if($returnId === true){
			$post_ids = is_array($post_ids) ? $post_ids : array();
			return $post_ids;
		}
		return $this->formatPost($post_ids);
	}

	public function formatPost($post_ids){
		if(empty($post_ids)) {
			return array();
		} else {
			if(count($post_ids) > 0 ) {
				$r = array();
				foreach($post_ids as $post_id) {
					$v = $this->getPostInfo($post_id);
					$r[] = $v;
				}
				return $r;
			} else {
				return array();
			}
		}
	}

	public function getPostDiggs($post_id,$count=10){
		$map = array('post_id'=>$post_id);
		$diggList = M('weiba_post_digg')->where($map)->order("id DESC")->findPage($count);
		for($i=0;$i<count($diggList['data']);$i++){
			$user_info = model('User')->getUserInfo($diggList['data'][$i]['uid']);
			$diggList['data'][$i]['uname'] = $user_info['uname'];
			$diggList['data'][$i]['avatar_big'] =$user_info['avatar_big'];
			$diggList['data'][$i]['avatar_middle'] =$user_info['avatar_middle'];
			$diggList['data'][$i]['avatar_small'] =$user_info['avatar_small'];

		}

		return $diggList;
	}

	public function addPostDigg($post_id,$uid){
		$maps['post_id'] = $map['post_id'] = intval($post_id);
		$map['uid'] = $uid;
		$hasdigg = M('weiba_post_digg')->where($map)->find();
		$weiba = M('weiba_post')->where('post_id='.$map['post_id'])->find();

		$map['cTime'] = time();
		$result = M('weiba_post_digg')->add($map);
		if( $result && !$hasdigg){
			$post = M('weiba_post')->where($maps)->find();
			M('weiba_post')->where($maps)->setField('praise',$post['praise']+1);
			return true;
		}else{
			return false;
		}
	}

	public function delPostDigg ($post_id,$uid){
		$maps['post_id'] = $post_id;
		$map['uid'] = $uid;
		$hasdigg = M('weiba_post')->where('post_id=' . $map['post_id'])->find();
		// $is_follow = $this->is_follow($hasdigg['weiba_id']);
		// if(!$is_follow){
		// 	echo 0;exit;
		// }

		$result = M('weiba_post_digg')->where($map)->delete();
		if ($result) {
			$post = M('weiba_post')->where($maps)->find();
			M('weiba_post')->where($maps)->setField('praise', $post['praise'] - 1);
			return true;
		} else {
			return false;
		}
	}
}